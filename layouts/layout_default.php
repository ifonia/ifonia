<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>.:: <?= $conf[$module]['name']; ?> | Ifonia ::. </title>
	<link rel="icon" href="img/favicon.ico" type="image/x-icon">

	<link rel="stylesheet" href="/css/bootstrap/bootstrap.css">
	<link rel="stylesheet" href="/css/font-awesome/font-awesome.css">
	<link rel="stylesheet" href="/css/ifonia.css">

	<script src="js/jquery.min.js"></script>
</head>
<body>
	<?php
		if (file_exists( $path_modules )) include( $path_modules );
		else die('Error al cargar el módulo <b>'.$module.'. No existe el archivo <b>'.$conf[$module]['file'].'</b>').PHP_EOL;
	?>
</body>
	<script src="js/core.min.js"></script>
	<script src="js/wispcenter.js"></script>
</html>