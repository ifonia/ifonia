<?php 
/**
 * Archivo de Configuracion de la pagina WispCenter
 */
define('DS', DIRECTORY_SEPARATOR);
define('MODULE_DEFAULT', 'home');
define('LAYOUT_HOME', 'layout_home.php');
define('LAYOUT_DEFAULT', 'layout_default.php');

define('MODULES_PATH', realpath('./modules/'));
define('LAYOUTS_PATH', realpath('./layouts/'));
define('INCLUDES_PATH',realpath('./includes/'));

$conf['home'] = array(
	'file'   => 'home.php',
	'name' => ' Bienvenidos',
	//'layout' => LAYOUT_HOME
	);

$conf['about'] = array(
	'file'   => 'about.php',
	'layout' => LAYOUT_DEFAULT
	);