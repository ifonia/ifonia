<?php

include('app/conf.php');

if (!empty($_GET['mod'])) {
	$module = $_GET['mod	'];
} else {
	$module = MODULE_DEFAULT;
}

if (empty($conf[$module])) {
	$module = MODULE_DEFAULT;
}

if (empty($conf[$module]['layout'])) {
	$conf[$module]['layout'] = LAYOUT_DEFAULT;
}

$path_layouts = LAYOUTS_PATH.'/'.$conf[$module]['layout'];
$path_modules = MODULES_PATH.'/'.$conf[$module]['file'];

if (file_exists($path_layouts)) {
	include($path_layouts);
} else {
	if (file_exists($path_modules)) {
		include($path_modules);
	} else {
		die('Error al cargar el módulo **'.$module.'**. No existe el archivo **'.$conf[$module]['file'].'**');
	}
}